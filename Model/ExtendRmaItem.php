<?php

namespace Sellry\MagentoRma\Model;

use Oro\Bundle\BusinessEntitiesBundle\Entity\BaseOrderItem;

class ExtendRmaItem extends BaseOrderItem
{
    /**
     * Constructor
     *
     * The real implementation of this method is auto generated.
     *
     * IMPORTANT: If the derived class has own constructor it must call parent constructor.
     */
    public function __construct()
    {
    }
}
