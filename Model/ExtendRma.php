<?php

namespace Sellry\MagentoRma\Model;

use Oro\Bundle\BusinessEntitiesBundle\Entity\BaseOrder;

class ExtendRma extends BaseOrder
{
    /**
     * Constructor
     *
     * The real implementation of this method is auto generated.
     *
     * IMPORTANT: If the derived class has own constructor it must call parent constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }
}
