<?php
// src/Sellry/MagentoRma/SellryMagentoRma.php
namespace Sellry\MagentoRma;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class SellryMagentoRma extends Bundle
{   
    public function getParent()
    {
        return 'OroCRMMagentoBundle';
    }
}