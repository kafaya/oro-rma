<?php

namespace Sellry\MagentoRma\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use Oro\Bundle\SecurityBundle\Annotation\Acl;
use Oro\Bundle\SecurityBundle\Annotation\AclAncestor;
use Oro\Bundle\IntegrationBundle\Entity\Channel;

use OroCRM\Bundle\MagentoBundle\Entity\Customer;
use Sellry\MagentoRma\Entity\Rma;

/**
 * @Route("/rma")
 */
class RmaController extends Controller
{
    /**
     * @Route("/", name="orocrm_magento_rma_index")
     * @AclAncestor("orocrm_magento_order_view")
     * @Template
     */
    public function indexAction()
    {
        return [
            'entity_class' => $this->container->getParameter('sellry.magentorma.entity.class')
        ];
    }
    
    /**
     * @Route("/view/{id}", name="orocrm_magento_rma_view", requirements={"id"="\d+"}))
     * @Acl(
     *      id="orocrm_magento_order_view",
     *      type="entity",
     *      permission="VIEW",
     *      class="OroCRMMagentoBundle:Order"
     * )
     * @Template
     * @param Rma $rma
     * @return array
     */
    public function viewAction(Rma $rma)
    {
        return ['entity' => $rma];
    }
    
    /**
     * @Route("/info/{id}", name="orocrm_magento_rma_widget_info", requirements={"id"="\d+"}))
     * @AclAncestor("orocrm_magento_cart_view")
     * @Template
     * @param Rma $rma
     * @return array
     */
    public function infoAction(Rma $rma)
    {
        return ['entity' => $rma];
    }
    
    /**
     * @Route("/widget/grid/{id}", name="sellry_magentorma_widget_items"))
     * @AclAncestor("orocrm_magento_cart_view")
     * @Template
     * @param Rma $rma
     * @return array
     */
    public function itemsAction(Rma $rma)
    {
        return ['entity' => $rma];
    }
    
     /**
     * @Route(
     *        "/customer-widget/customer-rma/{customerId}",
     *        name="orocrm_magento_customer_rma_widget",
     *        requirements={"customerId"="\d+"}
     * )
     * @ParamConverter("customer", class="OroCRMMagentoBundle:Customer", options={"id"="customerId"})
     * @Template
     * @param Customer $customer
     * @return array
     */
    public function customerRmaWidgetAction(Customer $customer)
    {
        return ['customer' => $customer];
    }


}
