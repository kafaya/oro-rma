<?php

namespace Sellry\MagentoRma\Migrations\Schema\v1_1;

use Doctrine\DBAL\Schema\Schema;
use Oro\Bundle\EntityExtendBundle\EntityConfig\ExtendScope;
use Oro\Bundle\MigrationBundle\Migration\Migration;
use Oro\Bundle\MigrationBundle\Migration\QueryBag;

/**
 * @SuppressWarnings(PHPMD.TooManyMethods)
 * @SuppressWarnings(PHPMD.ExcessiveClassLength)
 */
class SellryMagentoRma implements Migration
{
    /**
     * @inheritdoc
     */
    public function up(Schema $schema, QueryBag $queries)
    {
        
        // magento rma entity
        //$schema->dropTable('orocrm_magento_rma');
        $table = $schema->createTable('orocrm_magento_rma');
        $table->addColumn('id', 'integer', ['autoincrement' => true]);
        $table->addColumn('customer_id', 'integer',['notnull' => false]);
        $table->addColumn('order_id', 'integer',['notnull' => false]);
        $table->addColumn('store_id', 'integer',['notnull' => false]);
        $table->addColumn('user_owner_id', 'integer',['notnull' => false]);
        $table->addColumn('organization_id', 'integer',['notnull' => false]);
        $table->addColumn('channel_id', 'integer',['notnull' => false]);
        $table->addColumn('data_channel_id', 'integer',['notnull' => false]);
        $table->addColumn('increment_id', 'string', ['notnull' => false, 'length' => 255]);
        $table->addColumn('rma_id', 'string', ['length' => 255]);
        $table->addColumn('rma_status', 'string', ['notnull' => false, 'length' => 255]);
        $table->addColumn('package_opened', 'string', ['notnull' => false, 'length' => 255]);
        $table->addColumn('request_type', 'string', ['notnull' => false, 'length' => 255]);
        $table->addColumn('return_address', 'string', ['notnull' => false, 'length' => 255]);
        $table->addColumn('company', 'string', ['notnull' => false, 'length' => 255]);
        $table->addColumn('telephone', 'string', ['notnull' => false, 'length' => 255]);
        $table->addColumn('tracking_code', 'string', ['notnull' => false, 'length' => 255]);
        $table->addColumn('notes', 'string', ['notnull' => false, 'length' => 255]);
        $table->addColumn('customer_email', 'string', ['notnull' => false, 'length' => 255]);
        $table->addColumn('currency', 'string', ['notnull' => false, 'length' => 255]);
        $table->addColumn('payment_method', 'string', ['notnull' => false, 'length' => 255]);
        $table->addColumn('payment_details', 'string', ['notnull' => false, 'length' => 255]);
        $table->addColumn('subtotal_amount', 'money', ['notnull' => false]);
        $table->addColumn('shipping_amount', 'money', ['notnull' => false]);
        $table->addColumn('shipping_method', 'string', ['notnull' => false, 'length' => 255]);
        $table->addColumn('tax_amount', 'string', ['notnull' => false]);
        $table->addColumn('discount_amount', 'money', ['notnull' => false]);
        $table->addColumn('discount_percent', 'percent', ['notnull' => false]);
        $table->addColumn('total_amount', 'money', ['notnull' => false]);
        $table->addColumn('status', 'string', ['notnull' => false, 'length' => 255]);
        $table->addColumn('created_at', 'datetime', []);
        $table->addColumn('updated_at', 'datetime', []);
        $table->addColumn('first_name', 'string', ['notnull' => false, 'length' => 255]);
        $table->addColumn('last_name', 'string', ['notnull' => false, 'length' => 255]);
        $table->setPrimaryKey(['id']);
        $table->addUniqueIndex(['rma_id', 'channel_id'], 'unq_rma_id_channel_id');
        $table->addForeignKeyConstraint(
            $schema->getTable('orocrm_channel'),
            ['data_channel_id'],
            ['id'],
            ['onDelete' => 'SET NULL', 'onUpdate' => null]
        );
        $table->addIndex(['data_channel_id'], 'IDX_5537B98DBDC09B73', []);
        $table->addIndex(['created_at'], 'magerma_created_idx', []);
        $table->addForeignKeyConstraint(
            $schema->getTable('oro_integration_channel'),
            ['channel_id'],
            ['id'],
            ['onDelete' => 'SET NULL', 'onUpdate' => null]
        );
        $table->addIndex(['channel_id'], 'IDX_5537B98D72F5A1AA', []);
        $table->addForeignKeyConstraint(
            $schema->getTable('oro_organization'),
            ['organization_id'],
            ['id'],
            ['onDelete' => 'SET NULL', 'onUpdate' => null]
        );
        $table->addIndex(['organization_id'], 'IDX_5537B98D32C8A3DE', []);
        $table->addForeignKeyConstraint(
            $schema->getTable('oro_user'),
            ['user_owner_id'],
            ['id'],
            ['onDelete' => 'SET NULL', 'onUpdate' => null]
        );
        $table->addIndex(['user_owner_id'], 'IDX_5537B98D9EB185F9', []);
        $table->addForeignKeyConstraint(
            $schema->getTable('orocrm_magento_store'),
            ['store_id'],
            ['id'],
            ['onDelete' => 'SET NULL', 'onUpdate' => null]
        );
        $table->addIndex(['store_id'], 'IDX_5537B98DB092A811', []);
        $table->addForeignKeyConstraint(
            $schema->getTable('orocrm_magento_order'),
            ['order_id'],
            ['id'],
            ['onDelete' => 'SET NULL', 'onUpdate' => null]
        );
        $table->addIndex(['order_id'], 'IDX_5537B98D8D9F6D38', []);
        $table->addForeignKeyConstraint(
            $schema->getTable('orocrm_magento_customer'),
            ['customer_id'],
            ['id'],
            ['onDelete' => 'SET NULL', 'onUpdate' => null]
        );
        $table->addIndex(['customer_id'], 'IDX_5537B98D9395C3F3', []);
        
        
        //rma items entity
        //$schema->dropTable('orocrm_magento_rma_items');
        $table = $schema->createTable('orocrm_magento_rma_items');
        $table->addColumn('id', 'integer', ['autoincrement' => true]);
        $table->addColumn('order_id', 'integer',['notnull' => false]);
        $table->addColumn('owner_id', 'integer',['notnull' => false]);
        $table->addColumn('channel_id', 'integer',['notnull' => false]);
        $table->addColumn('rma_id', 'string',['notnull' => false , 'length' => 255]);
        $table->addColumn('product_type', 'string',['notnull' => false , 'length' => 255]);
        $table->addColumn('product_options', 'string',['notnull' => false , 'length' => 255]);
        $table->addColumn('is_virtual', 'string',['notnull' => false , 'length' => 255]);
        $table->addColumn('original_price', 'money',['notnull' => false]);
        $table->addColumn('discount_percent', 'money',['notnull' => false]);
        $table->addColumn('name', 'string',['notnull' => false , 'length' => 255]);
        $table->addColumn('sku', 'string',['notnull' => false , 'length' => 255]);
        $table->addColumn('qty', 'string',['notnull' => false , 'length' => 255]);
        $table->addColumn('price', 'money',['notnull' => false]);
        $table->addColumn('weight', 'string',['notnull' => false , 'length' => 255]);
        $table->addColumn('tax_percent', 'money',['notnull' => false]);
        $table->addColumn('tax_amount', 'money',['notnull' => false]);
        $table->addColumn('discount_amount', 'money',['notnull' => false]);
        $table->addColumn('row_total', 'money',['notnull' => false]);
        $table->addColumn('origin_id', 'string',['notnull' => false , 'length' => 255]);
        $table->setPrimaryKey(['id']);
        $table->addForeignKeyConstraint(
            $schema->getTable('orocrm_magento_order'),
            ['order_id'],
            ['id'],
            ['onDelete' => 'SET NULL', 'onUpdate' => null]
        );
        $table->addIndex(['order_id'], 'IDX_D35444F18D9F6D38', []);
        $table->addForeignKeyConstraint(
            $schema->getTable('oro_integration_channel'),
            ['channel_id'],
            ['id'],
            ['onDelete' => 'SET NULL', 'onUpdate' => null]
        );
        $table->addIndex(['channel_id'], 'IDX_D35444F172F5A1AA', []);
                $table->addForeignKeyConstraint(
            $schema->getTable('oro_user'),
            ['owner_id'],
            ['id'],
            ['onDelete' => 'SET NULL', 'onUpdate' => null]
        );
        $table->addIndex(['owner_id'], 'IDX_D35444F17E3C61F9', []);
        
    }

}
