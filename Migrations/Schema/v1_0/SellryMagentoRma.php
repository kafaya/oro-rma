<?php

namespace Sellry\MagentoRma\Migrations\Schema\v1_0;

use Doctrine\DBAL\Schema\Schema;
use Oro\Bundle\EntityExtendBundle\EntityConfig\ExtendScope;
use Oro\Bundle\MigrationBundle\Migration\Migration;
use Oro\Bundle\MigrationBundle\Migration\QueryBag;

/**
 * @SuppressWarnings(PHPMD.TooManyMethods)
 * @SuppressWarnings(PHPMD.ExcessiveClassLength)
 */
class SellryMagentoRma implements Migration
{
    /**
     * @inheritdoc
     */
    public function up(Schema $schema, QueryBag $queries)
    {
 // magento customer entity
        $table = $schema->getTable('orocrm_magento_customer');
        //$table->dropColumn('company');
        //$table->dropColumn('handlabCustomerId');
        //$table->dropColumn('customerNotes');
        //$table->dropColumn('creditLimit');
        //$table->dropColumn('credentialsTitle');
        //$table->dropColumn('phoneNumber');
        //$table->dropColumn('apEmail');
        //$table->dropColumn('invoicePreference');
        $table->addColumn(
            'company',
            'string',
            array('oro_options' => array(
                'entity' => array('label' => 'Company'),
                'extend' => array('is_extend' => true, 'owner' => ExtendScope::OWNER_SYSTEM,'length' => 255)
            ))
        ); 
        
        $table->addColumn(
            'handlabCustomerId',
            'string',
            array('oro_options' => array(
                'entity' => array('label' => 'Magento Customer Id'),
                'extend' => array('is_extend' => true, 'owner' => ExtendScope::OWNER_SYSTEM)
            ))
        ); 
        
        $table->addColumn(
            'customerNotes',
            'string',
            array('oro_options' => array(
                'entity' => array('label' => 'Customer Notes'),
                'extend' => array('is_extend' => true, 'owner' => ExtendScope::OWNER_SYSTEM,'length' => 500)
            ))
        ); 
        
        $table->addColumn(
            'creditLimit',
            'string',
            array('oro_options' => array(
                'entity' => array('label' => 'Credit Limit'),
                'extend' => array('is_extend' => true, 'owner' => ExtendScope::OWNER_SYSTEM)
            ))
        ); 
        
        $table->addColumn(
            'credentialsTitle',
            'string',
            array('oro_options' => array(
                'entity' => array('label' => 'Credentials/Title'),
                'extend' => array('is_extend' => true, 'owner' => ExtendScope::OWNER_SYSTEM,'length' => 500)
            ))
        );
        
        $table->addColumn(
            'phoneNumber',
            'string',
            array('oro_options' => array(
                'entity' => array('label' => 'Phone Number'),
                'extend' => array('is_extend' => true, 'owner' => ExtendScope::OWNER_SYSTEM)
            ))
        );
        
        $table->addColumn(
            'apEmail',
            'string',
            array('oro_options' => array(
                'entity' => array('label' => 'AP Email'),
                'extend' => array('is_extend' => true, 'owner' => ExtendScope::OWNER_SYSTEM)
            ))
        );

        $table->addColumn(
            'invoicePreference',
            'string',
            array('oro_options' => array(
                'entity' => array('label' => 'Invoice Preference'),
                'extend' => array('is_extend' => true, 'owner' => ExtendScope::OWNER_SYSTEM)
            ))
        );
        
    }

}
