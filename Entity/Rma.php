<?php

namespace Sellry\MagentoRma\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

use Oro\Bundle\EntityConfigBundle\Metadata\Annotation\ConfigField;
use Oro\Bundle\OrganizationBundle\Entity\Organization;
use Oro\Bundle\UserBundle\Entity\User;
use Oro\Bundle\EmailBundle\Entity\Email;
use Oro\Bundle\AddressBundle\Entity\AddressType;
use Oro\Bundle\AddressBundle\Entity\AbstractTypedAddress;
use Oro\Bundle\WorkflowBundle\Entity\WorkflowItem;
use Oro\Bundle\WorkflowBundle\Entity\WorkflowStep;
use Oro\Bundle\LocaleBundle\Model\FirstNameInterface;
use Oro\Bundle\LocaleBundle\Model\LastNameInterface;
use Oro\Bundle\EntityConfigBundle\Metadata\Annotation\Config;

use OroCRM\Bundle\CallBundle\Entity\Call;
use Sellry\MagentoRma\Model\ExtendRma;
use OroCRM\Bundle\ChannelBundle\Model\ChannelAwareInterface;


/**
 * Class Rma
 *
 * @package Sellry\MagentoRma\Entity
 * @ORM\Entity(repositoryClass="Sellry\MagentoRma\Entity\Repository\OrderRepository")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="orocrm_magento_rma",
 *     indexes={
 *          @ORM\Index(name="magerma_created_idx",columns={"created_at"})
 *     },
 *     uniqueConstraints={
 *          @ORM\UniqueConstraint(name="unq_rma_id_channel_id", columns={"rma_id", "channel_id"})
 *     }
 * )
 * @Config(
 *      routeView="orocrm_magento_rma_view",
 *      defaultValues={
 *          "entity"={
 *              "icon"="icon-list-alt",
 *              "context-grid"="magento-order-for-context-grid"
 *          },
 *          "ownership"={
 *              "owner_type"="USER",
 *              "owner_field_name"="owner",
 *              "owner_column_name"="user_owner_id",
 *              "organization_field_name"="organization",
 *              "organization_column_name"="organization_id"
 *          },
 *          "security"={
 *              "type"="ACL",
 *              "group_name"=""
 *          }
 *      }
 * )
 * @SuppressWarnings(PHPMD.ExcessivePublicCount)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 */
class Rma extends ExtendRma implements
    ChannelAwareInterface,
    FirstNameInterface,
    LastNameInterface,
    IntegrationAwareInterface
{
    const STATUS_CANCELED = 'canceled';

    use IntegrationEntityTrait, NamesAwareTrait, ChannelEntityTrait;

    /**
     * @var string
     *
     * @ORM\Column(name="increment_id", type="string", length=60, nullable=false)
     * @ConfigField(
     *      defaultValues={
     *          "importexport"={
     *              "identity"=true
     *          }
     *      }
     * )
     */
    protected $incrementId;

    /**
     * @var string
     *
     * @ORM\Column(name="rma_id", type="string", length=255, nullable=true)
     */ 
     
    protected $rmaId;

    /**
     * @var string
     *
     * @ORM\Column(name="rma_status", type="string", length=255, nullable=true)
     */ 
    
    protected $rmaStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="package_opened", type="string", length=255, nullable=true)
     */ 
    
    protected $packageOpened;

    /**
     * @var string
     *
     * @ORM\Column(name="request_type", type="string", length=255, nullable=true)
     */ 
    
    protected $requestType;

    /**
     * @var string
     *
     * @ORM\Column(name="return_address", type="text", nullable=true)
     */  
     
    protected $returnAddress;
       
    /**
     * @var string
     *
     * @ORM\Column(name="company", type="string", length=255, nullable=true)
     */  
     
    protected $company;
    
    /**
     * @var string
     *
     * @ORM\Column(name="telephone", type="string", length=255, nullable=true)
     */  
     
    protected $telephone;

    /**
     * @var string
     *
     * @ORM\Column(name="tracking_code", type="string", length=255, nullable=true)
     */
     
    protected $trackingCode;      
    
    /**
     * @var Customer
     *
     * @ORM\ManyToOne(targetEntity="OroCRM\Bundle\MagentoBundle\Entity\Customer")
     * @ORM\JoinColumn(name="customer_id", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     */
    protected $customer;
    
    /**
     * @var Order
     *
     * @ORM\ManyToOne(targetEntity="OroCRM\Bundle\MagentoBundle\Entity\Order")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     */
    protected $order;
    
    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="OroCRM\Bundle\MagentoBundle\Entity\Address",
     *     mappedBy="owner", cascade={"all"}, orphanRemoval=true
     * )
     * @ConfigField(
     *      defaultValues={
     *          "importexport"={
     *              "full"=true
     *          }
     *      }
     * )
     */
    protected $addresses;
    
    /**
     * @var Store
     *
     * @ORM\ManyToOne(targetEntity="OroCRM\Bundle\MagentoBundle\Entity\Store")
     * @ORM\JoinColumn(name="store_id", referencedColumnName="id", onDelete="SET NULL")
     * @ConfigField(
     *      defaultValues={
     *          "importexport"={
     *              "full"=false
     *          }
     *      }
     * )
     */
    protected $store;
    
    /**
     * @var OrderItem[]|Collection
     *
     * @ORM\OneToMany(targetEntity="OroCRM\Bundle\MagentoBundle\Entity\OrderItem", mappedBy="order",cascade={"all"})
     * @ConfigField(
     *      defaultValues={
     *          "importexport"={
     *              "full"=true
     *          }
     *      }
     * )
     */
    protected $items;

    /**
     * @var string
     *
     * @ORM\Column(name="notes", type="text", nullable=true)
     */
    protected $notes;

    /**
     * @var string
     *
     * @ORM\Column(name="customer_email", type="string", length=255, nullable=true)
     */
    protected $customerEmail;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="Oro\Bundle\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_owner_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $owner;

    /**
     * @var Organization
     *
     * @ORM\ManyToOne(targetEntity="Oro\Bundle\OrganizationBundle\Entity\Organization")
     * @ORM\JoinColumn(name="organization_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $organization;


    public function __construct()
    {
        parent::__construct();

        $this->items         = new ArrayCollection();
    }

    public function setIncrementId($incrementId)
    {
        $this->incrementId = $incrementId;

        return $this;
    }

    /**
     * @return string
     */
    public function getIncrementId()
    {
        return $this->incrementId;
    }
    
    /**
     * @return string
     */
    public function getOrder()
    {
        return $this->order;
    }
    
    /**
     * @return string
     */
    public function setOrder($order)
    {
        return $this->order;
    }
    
    /**
     * @return string
     */
    public function getCustomer()
    {
        return $this->customer;
    }
    
    /**
     * @return string
     */
    public function setCustomer($customer)
    {
        return $this->$customer;
    }
    
    /**
     * @param string $rmaId
     *
     * @return Order
     */
    public function setRmaId($rmaId)
    {
        $this->rmaId = $rmaId;

        return $this;
    }

    /**
     * @return string
     */
    public function getRmaId()
    {
        return $this->rmaId;
    }
    
     /**
     * @param string $returnAddress
     *
     * @return Order
     */
    public function setReturnAddress($returnAddress)
    {
        $this->returnAddress = $returnAddress;

        return $this;
    }

    /**
     * @return string
     */
    public function getReturnAddress()
    {
        return $this->returnAddress;
    }
    
    
    /**
     * @param string $company
     *
     * @return Order
     */
    public function setCompany($company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * @return string
     */
    public function getCompany()
    {
        return $this->company;
    }
    
     /**
     * @param string $telephone
     *
     * @return Order
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }
    
    /**
     * @param string $rmaStatus
     *
     * @return Order
     */
    public function setRmaStatus($rmaStatus)
    {
        $this->rmaStatus = $rmaStatus;

        return $this;
    }

    /**
     * @return string
     */
    public function getRmaStatus()
    {
        return $this->rmaStatus;
    }
    
    
    /**
     * @param string $packageOpened
     *
     * @return Order
     */
    public function setPackageOpened($packageOpened)
    {
        $this->packageOpened = $packageOpened;

        return $this;
    }

    /**
     * @return string
     */
    public function getPackageOpened()
    {
        return $this->packageOpened;
    }
    
    /**
     * @param string $requestType
     *
     * @return Order
     */
    public function setRequestType($requestType)
    {
        $this->requestType = $requestType;

        return $this;
    }

    /**
     * @return string 
     */
    public function getRequestType()
    {
        return $this->requestType;
    }
    
    /**
     * @param string $trackingCode
     *
     * @return Order
     */
    public function setTrackingCode($trackingCode)
    {
        $this->trackingCode = $trackingCode;

        return $this;
    }

    /**
     * @return string $trackingCode
     */
    public function getTrackingCode()
    {
        return $this->trackingCode;
    }

    /**
     * @return Store
     */
    public function getStore()
    {
        return $this->store;
    }

    /**
     * @param string $storeName
     *
     * @return Order
     */
    public function setStoreName($storeName)
    {
        $this->storeName = $storeName;

        return $this;
    }

    /**
     * @return string
     */
    public function getStoreName()
    {
        return $this->storeName;
    }

    /**
     * @param float $totalCanceledAmount
     *
     * @return Order
     */
    public function setTotalCanceledAmount($totalCanceledAmount)
    {
        $this->totalCanceledAmount = $totalCanceledAmount;

        return $this;
    }

    /**
     * @return float
     */
    public function getTotalCanceledAmount()
    {
        return $this->totalCanceledAmount;
    }

    /**
     * @param float $totalInvoicedAmount
     *
     * @return Order
     */
    public function setTotalInvoicedAmount($totalInvoicedAmount)
    {
        $this->totalInvoicedAmount = $totalInvoicedAmount;

        return $this;
    }

    /**
     * @return float
     */
    public function getTotalInvoicedAmount()
    {
        return $this->totalInvoicedAmount;
    }

    /**
     * @param float $totalPaidAmount
     *
     * @return Order
     */
    public function setTotalPaidAmount($totalPaidAmount)
    {
        $this->totalPaidAmount = $totalPaidAmount;

        return $this;
    }

    /**
     * @return float
     */
    public function getTotalPaidAmount()
    {
        return $this->totalPaidAmount;
    }

    /**
     * @param float $totalRefundedAmount
     *
     * @return Order
     */
    public function setTotalRefundedAmount($totalRefundedAmount)
    {
        $this->totalRefundedAmount = $totalRefundedAmount;

        return $this;
    }

    /**
     * @return float
     */
    public function getTotalRefundedAmount()
    {
        return $this->totalRefundedAmount;
    }

    /**
     * @param string $remoteIp
     *
     * @return Order
     */
    public function setRemoteIp($remoteIp)
    {
        $this->remoteIp = $remoteIp;

        return $this;
    }

    /**
     * @return string
     */
    public function getRemoteIp()
    {
        return $this->remoteIp;
    }

    /**
     * @param Cart $cart
     *
     * @return Order
     */
    public function setCart($cart = null)
    {
        $this->cart = $cart;

        return $this;
    }

    /**
     * @return Cart
     */
    public function getCart()
    {
        return $this->cart;
    }

    /**
     * @param string $notes
     *
     * @return Order
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * @param string $feedback
     *
     * @return Order
     */
    public function setFeedback($feedback)
    {
        $this->feedback = $feedback;

        return $this;
    }

    /**
     * @return string
     */
    public function getFeedback()
    {
        return $this->feedback;
    }

    /**
     * Pre persist event listener
     *
     * @ORM\PrePersist
     */
    public function beforeSave()
    {
        $this->updateNames();
    }

    /**
     * Pre update event handler
     *
     * @ORM\PreUpdate
     */
    public function doPreUpdate()
    {
        $this->updateNames();
    }

    /**
     * {@inheritdoc}
     */
    public function getBillingAddress()
    {
        $addresses = $this->getAddresses()->filter(
            function (AbstractTypedAddress $address) {
                return $address->hasTypeWithName(AddressType::TYPE_BILLING);
            }
        );

        return $addresses->first();
    }

    /**
     * @param string $customerEmail
     *
     * @return Order
     */
    public function setCustomerEmail($customerEmail)
    {
        $this->customerEmail = $customerEmail;

        return $this;
    }

    /**
     * @return string
     */
    public function getCustomerEmail()
    {
        return $this->customerEmail;
    }

    /**
     * @return User
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param User $user
     */
    public function setOwner(User $user)
    {
        $this->owner = $user;
    }

    /**
     * Set organization
     *
     * @param Organization $organization
     * @return Order
     */
    public function setOrganization(Organization $organization = null)
    {
        $this->organization = $organization;

        return $this;
    }

    /**
     * Get organization
     *
     * @return Organization
     */
    public function getOrganization()
    {
        return $this->organization;
    }
}
