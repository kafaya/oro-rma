<?php

namespace Sellry\MagentoRma\Entity;

interface OriginAwareInterface
{
    /**
     * @param int $originId
     *
     * @return OriginAwareInterface
     */
    public function setOriginId($originId);

    /**
     * @return int
     */
    public function getOriginId();
}
